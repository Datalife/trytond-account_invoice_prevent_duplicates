# This file is part account_invoice_prevent_duplicates module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.transaction import Transaction
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Bool
from trytond.i18n import gettext
from trytond.exceptions import UserError, UserWarning
from itertools import groupby


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        # Reference should be required to detect duplicates
        if 'type' not in cls.reference.depends:
            old_required = cls.reference.states.get('required', Bool(False))
            cls.reference.states.update({
                    'required': old_required | ((Eval('type') == 'in')
                        & ~Eval('state').in_(['draft', 'cancelled']))
                    })
            cls.reference.depends.append('type')

    @property
    def duplicate_invoice_domain(self):
        domain = []
        domain.append(('party', '=', self.party.id))
        domain.append(('type', '=', self.type))
        domain.append(('reference', '=', self.reference))
        domain.append(('state', 'in', ('posted', 'paid', 'validated')))
        domain.append(('company', '=', self.company.id))
        domain.append(('id', '!=', self.id))
        return domain

    @classmethod
    def set_number(cls, invoices):

        super().set_number(invoices)

        def keyfunc(invoice):
            return [getattr(invoice, domain[0])
                for domain in invoice.duplicate_invoice_domain[:-1]]

        in_invoices = [i for i in invoices if i.type == 'in']
        draft_duplicated_invoices = []
        in_invoices = sorted(in_invoices, key=keyfunc)
        for key, grouped_invoices in groupby(in_invoices, key=keyfunc):
            grouped_invoices = list(grouped_invoices)
            if len(grouped_invoices) > 1:
                draft_duplicated_invoices.extend(grouped_invoices)

        if draft_duplicated_invoices:
            cls.show_duplicated_invoices(draft_duplicated_invoices)
        else:
            for invoice in in_invoices:
                duplicated_invoices = cls.search(
                    invoice.duplicate_invoice_domain)
                if duplicated_invoices:
                    cls.show_duplicated_invoices([invoice]
                        + duplicated_invoices)

    @property
    def duplicate_invoice_date(self):
        return self.invoice_date

    @classmethod
    def show_duplicated_invoices(cls, invoices):
        if not invoices:
            return

        pool = Pool()
        Warning = pool.get('res.user.warning')
        language = Transaction().language
        error = gettext(
            'account_invoice_prevent_duplicates.party_invoice_reference',
            language)
        text = []
        for invoice in invoices:
            text.append(error % {
                    'invoice': invoice.rec_name or '',
                    'party': invoice.party.name,
                    'reference': invoice.reference,
                    })
        text = '\n\n'.join(text)
        warning_name = 'duplicate_invoice_%s' % invoices[0].id
        dates = [invoice.duplicate_invoice_date for invoice in set(invoices)]
        if len(dates) > len(set(dates)):
            raise UserError(gettext(
                'account_invoice_prevent_duplicates.duplicate_invoice',
                message=text))
        elif Warning.check(warning_name):
            raise UserWarning(warning_name,
                gettext(
                    'account_invoice_prevent_duplicates.duplicate_invoice',
                    message=text))
